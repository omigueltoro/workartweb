import { PaintingType } from './PaintingType';
import { DamageCategory } from './DamageCategory';
import { DamageDepthLevel } from './DamageDepthLevel';

export class Cost{
    id:number;
    cost:number;
    paintingType: PaintingType;
    damageCategory: DamageCategory;
    damageDepthLevel: DamageDepthLevel;
    paintingTypeCol: string;
    damageCategoryCol: string;
    damageDepthLevelCol: string;
    
    constructor(id:number = null,cost:number = null,paintingType:PaintingType=null,damageCategory:DamageCategory=null,damageDepthLevel:DamageDepthLevel=null,
                paintingTypeCol: string = null, damageCategoryCol: string = null, damageDepthLevelCol: string = null){
        
    }
}