import { Component, OnInit } from '@angular/core';
import { Cost } from 'src/models/Cost';
import { CostService } from '../service/cost/cost.service';
import { MenuItem, ConfirmationService, SelectItem } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { DamageCategoryService } from '../service/damage-category/damage-category.service';
import { DamageDepthLevelService } from '../service/damage-depth-level/damage-depth-level.service';
import { PaintingTypeService } from '../service/painting-type/painting-type.service';
import { DamageCategory } from 'src/models/DamageCategory';
import { DamageDepthLevel } from 'src/models/DamageDepthLevel';
import { PaintingType } from 'src/models/PaintingType';

@Component({
  selector: 'app-costs',
  templateUrl: './costs.component.html',
  styleUrls: ['./costs.component.css']
})
export class CostsComponent implements OnInit {

  public title: string = "Costs";
  costs: Cost[];
  cols: any[];
  items: MenuItem[];
  displayCostSaveDialog: boolean = false;
  cost: Cost = {
    id: null,
    damageCategory: null,
    damageDepthLevel: null,
    paintingType: null,
    damageCategoryCol: null,
    damageDepthLevelCol: null,
    paintingTypeCol: null,
    cost: null
  };
  selectedCost: Cost = {
    id: null,
    damageCategory: null,
    damageDepthLevel: null,
    paintingType: null,
    damageCategoryCol: null,
    damageDepthLevelCol: null,
    paintingTypeCol: null,
    cost: null
  }
  public damageCategories: SelectItem[];
  public damageDepthLevels: SelectItem[];
  public paintingTypes: SelectItem[];

  constructor(private CostService: CostService, private messageService: MessageService, private confirmService: ConfirmationService,
    private DamageCategoryService: DamageCategoryService, private DamageDepthLevelService: DamageDepthLevelService, private PaintingTypeService: PaintingTypeService) { }

  ngOnInit(): void {
    var that = this;

    this.getAll();
    this.cols = [
      { field: "id", header: "Item" },
      { field: "paintingTypeCol", header: "Painting Type" },
      { field: "damageCategoryCol", header: "Damage Category" },
      { field: "damageDepthLevelCol", header: "Damage Depth Level" },
      { field: "cost", header: "Cost" },
    ];
    this.items = [
      {
        label: "New",
        icon: "pi pi-fw pi-plus",
        command: () => this.showSaveDialog(false)
      },
      {
        label: "Edit",
        icon: "pi pi-fw pi-pencil",
        command: () => this.selectedCost.id != null ? this.showSaveDialog(true) : this.messageService.add({ severity: 'warn', summary: "Result", detail: "Item not selected" })
      },
      {
        label: "Delete",
        icon: "pi pi-fw pi-times",
        command: () => this.delete()
      }
    ];
    this.DamageDepthLevelService.getAll().subscribe(
      (result: DamageDepthLevel[]) => {
        if (result) {
          let selectItem: SelectItem [] = []
          for(let i = 0; i < result.length; i++){
            
            let item: SelectItem = { label: result[i].descripcion, value: result[i] }
            selectItem.push(item);
          }
          this.damageDepthLevels = selectItem;
        }
      },
      error => {
        console.log(error);
      }
    );
    this.DamageCategoryService.getAll().subscribe(
      (result: DamageCategory[]) => {
        if (result) {
          let selectItem: SelectItem [] = []
          for(let i = 0; i < result.length; i++){
            
            let item: SelectItem = { label: result[i].descripcion, value: result[i] }
            selectItem.push(item);
          }
          this.damageCategories = selectItem;
        }
      },
      error => {
        console.log(error);

      }
    );
    this.PaintingTypeService.getAll().subscribe(
      (result: PaintingType[]) => {
        if (result) {
          let selectItem: SelectItem [] = []
          for(let i = 0; i < result.length; i++){
            
            let item: SelectItem = { label: result[i].descripcion, value: result[i] }
            selectItem.push(item);
          }
          this.paintingTypes = selectItem;
        }
      },
      error => {
        console.log(error);

      }
    );

  }

  getAll() {
    this.CostService.getAll().subscribe(
      (result: any) => {
        let costs: any[] = [];
        for (let i = 0; i < result.length; i++) {
          let cost = result[i];
          cost.paintingTypeCol = result[i].paintingType.descripcion;
          cost.damageCategoryCol = result[i].damageCategory.descripcion;
          cost.damageDepthLevelCol = result[i].damageDepthLevel.descripcion;
          costs.push(cost);
        }
        this.costs = costs;
      },
      error => {
        console.log(error);
      }

    )
  }
  showSaveDialog(editar: boolean): void {
    if (editar) {
      this.cost = this.selectedCost;
    }
    else {
      this.cost = {
        id: null,
        damageCategory: null,
        damageDepthLevel: null,
        paintingType: null,
        damageCategoryCol: null,
        damageDepthLevelCol: null,
        paintingTypeCol: null,
        cost: null
      };
    }
    this.displayCostSaveDialog = true;
  }
  save() {
    this.CostService.save(this.cost).subscribe(
      (result: any) => {
        
        let cost : Cost = result;
        cost.paintingTypeCol = result.paintingType.descripcion;
        cost.damageDepthLevelCol = result.damageDepthLevel.descripcion;
        cost.damageCategoryCol = result.damageCategory.descripcion;
        let index = this.costs.findIndex(p => p.id == cost.id);
        index != -1 ? this.costs[index] = cost : this.costs.push(cost);
        this.messageService.add({ severity: 'success', summary: "Result", detail: "Successful transaction" })
        this.displayCostSaveDialog = false;
      },
      error => {
        console.log(error);
      }
    )
  }
  delete() {
    if (this.selectedCost == null || this.selectedCost.id == null) {
      this.messageService.add({ severity: 'warn', summary: "Result", detail: "Item not selected" })
      return;
    }
    this.confirmService.confirm({
      message: "Are you sure you want to delete the record?",
      accept: () => {
        this.CostService.delete(this.selectedCost.id).subscribe(
          (result: any) => {
            this.messageService.add({ severity: 'success', summary: "Result", detail: "Successful transaction" })
            this.deleteObject(result.id)
          }
        )
      }
    })
  }

  deleteObject(id: number) {
    let index = this.costs.findIndex(p => p.id == id);
    if (index != -1) {
      this.costs.splice(index, 1)

    }
  }

  onSelectDamageCategory(damageCategory: any){
    this.cost.damageCategory = damageCategory;
  }
  onSelectDamageDepthLevel(damageDepthLevel: any){
    this.cost.damageDepthLevel = damageDepthLevel;
  }
  onSelectPaintingType(paintingType: any){
    this.cost.paintingType = paintingType;
  }
}
