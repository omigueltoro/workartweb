import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DamageDepthLevelsComponent } from './damage-depth-levels.component';

describe('DamageDepthLevelsComponent', () => {
  let component: DamageDepthLevelsComponent;
  let fixture: ComponentFixture<DamageDepthLevelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DamageDepthLevelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DamageDepthLevelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
