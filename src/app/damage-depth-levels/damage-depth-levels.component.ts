import { Component, OnInit } from '@angular/core';
import { DamageDepthLevel } from 'src/models/DamageDepthLevel';
import { DamageDepthLevelService } from '../service/damage-depth-level/damage-depth-level.service';
import { MenuItem, ConfirmationService } from 'primeng/api';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-damage-depth-levels',
  templateUrl: './damage-depth-levels.component.html',
  styleUrls: ['./damage-depth-levels.component.css'],
})
export class DamageDepthLevelsComponent implements OnInit {
  
  public title : string = "Damage Depth Levels";
  damageDepthLevels : DamageDepthLevel[];
  cols : any [];
  items: MenuItem [];
  displayDlevelSaveDialog : boolean = false;
  damageDepthLevel : DamageDepthLevel = {
    id: null,
    descripcion : null
  };
  selectedDamageDepthLevel: DamageDepthLevel = {
    id: null,
    descripcion : null
  }

  constructor(private DamageDepthLevelsService: DamageDepthLevelService, private messageService: MessageService, private confirmService : ConfirmationService) { }

  ngOnInit(): void {
    this.getAll();
    this.cols = [
      {field: "id", header: "Damage depth level"},
      {field: "descripcion", header: "Description"},
    ];
    this.items = [
      {
        label : "New",
        icon : "pi pi-fw pi-plus",
        command : () => this.showSaveDialog(false)
      },
      {
        label : "Edit",
        icon : "pi pi-fw pi-pencil",
        command : () => this.selectedDamageDepthLevel.id !=null ? this.showSaveDialog(true) : this.messageService.add({severity: 'warn', summary: "Result", detail: "Item not selected"})
      },
      {
        label : "Delete",
        icon : "pi pi-fw pi-times",
        command : () => this.delete()
      }
    ]
  }

  getAll(){
    this.DamageDepthLevelsService.getAll().subscribe(
      (result: any) => {
        let DamageDepthLevels : DamageDepthLevel[] = [];
        for(let i =0; i < result.length; i++){
          let damageDepthLevel = result[i] as DamageDepthLevel;
          DamageDepthLevels.push(damageDepthLevel);
        }
        this.damageDepthLevels = DamageDepthLevels;
      },
      error => {
        console.log(error);
      }
      
    )
  }
  showSaveDialog(editar:boolean): void {
    if(editar){
      this.damageDepthLevel = this.selectedDamageDepthLevel;
    }
    else{
      this.damageDepthLevel = {
        id: null,
        descripcion : null
      };
    }
    this.displayDlevelSaveDialog = true;
  }
  save(){
    this.DamageDepthLevelsService.save(this.damageDepthLevel).subscribe(
      (result:any) => {
        let damageDepthLevel = result as DamageDepthLevel;
        let index = this.damageDepthLevels.findIndex(p => p.id == damageDepthLevel.id);
        index != -1 ? this.damageDepthLevels[index] = damageDepthLevel : this.damageDepthLevels.push(damageDepthLevel);
        this.messageService.add({severity: 'success', summary: "Result", detail: "Successful transaction"})
        this.displayDlevelSaveDialog = false;
      },
      error => {
        console.log(error);
      }
    )
  }
  delete(){
    if(this.selectedDamageDepthLevel == null || this.selectedDamageDepthLevel.id == null){
      this.messageService.add({severity: 'warn', summary: "Result", detail: "Item not selected"})
      return;
    }
    this.confirmService.confirm({
      message: "Are you sure you want to delete the record?",
      accept : () => {
        this.DamageDepthLevelsService.delete(this.selectedDamageDepthLevel.id).subscribe(
          (result : any) => {
            this.messageService.add({severity: 'success', summary: "Result", detail: "Successful transaction"})
            this.deleteObject(result.id)
          }
        )
      }
    })
  }

  deleteObject (id:number) {
    let index = this.damageDepthLevels.findIndex(p => p.id == id);
    if(index != -1){
      this.damageDepthLevels.splice(index, 1)

    }
  }
}
