import { TestBed } from '@angular/core/testing';

import { DamageCategoryService } from './damage-category.service';

describe('DamagecategoryService', () => {
  let service: DamageCategoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DamageCategoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
