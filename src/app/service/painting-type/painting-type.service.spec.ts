import { TestBed } from '@angular/core/testing';

import { PaintingTypesService } from './painting-type.service';

describe('PaintingtypesService', () => {
  let service: PaintingTypesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PaintingTypesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
