import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PaintingType } from 'src/models/PaintingType';

@Injectable({
  providedIn: 'root'
})
export class PaintingTypeService {

  baseUrl:string = "http://localhost:8080/api/v1"

  constructor(private http:HttpClient) { }

  getAll() : Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
      return this.http.get(this.baseUrl+"/painting-types", {headers : headers});
  }
  save(paintingType: PaintingType): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    return this.http.post(this.baseUrl+"/painting-types", JSON.stringify(paintingType), {headers : headers});
  }
  delete(id:number) : Observable<any>{

    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    return this.http.delete(this.baseUrl+"/painting-types/"+id, {headers : headers});
  }
}
