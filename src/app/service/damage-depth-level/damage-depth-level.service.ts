import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DamageDepthLevel } from 'src/models/DamageDepthLevel';

@Injectable({
  providedIn: 'root'
})
export class DamageDepthLevelService {

  baseUrl:string = "http://localhost:8080/api/v1"

  constructor(private http:HttpClient) { }

  getAll() : Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
      return this.http.get(this.baseUrl+"/damage-depth-levels", {headers : headers});
  }
  save(DamageDepthLevel: DamageDepthLevel): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    return this.http.post(this.baseUrl+"/damage-depth-levels", JSON.stringify(DamageDepthLevel), {headers : headers});
  }
  delete(id:number) : Observable<any>{

    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    return this.http.delete(this.baseUrl+"/damage-depth-levels/"+id, {headers : headers});
  }
}
