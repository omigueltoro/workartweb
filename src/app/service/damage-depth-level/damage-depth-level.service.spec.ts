import { TestBed } from '@angular/core/testing';

import { DamageDepthLevelService } from './damage-depth-level.service';

describe('DamageDepthLevelService', () => {
  let service: DamageDepthLevelService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DamageDepthLevelService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
