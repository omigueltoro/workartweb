import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Cost } from 'src/models/Cost';

@Injectable({
  providedIn: 'root'
})
export class CostService {

  baseUrl:string = "http://localhost:8080/api/v1"

  constructor(private http:HttpClient) { }

  getAll() : Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
      return this.http.get(this.baseUrl+"/costs", {headers : headers});
  }
  save(Cost: Cost): Observable<any> {
    
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    return this.http.post(this.baseUrl+"/costs", JSON.stringify(Cost), {headers : headers});
  }
  delete(id:number) : Observable<any>{

    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    return this.http.delete(this.baseUrl+"/costs/"+id, {headers : headers});
  }
}
