import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DamageDepthLevelsComponent} from './damage-depth-levels/damage-depth-levels.component';
import {PaintingtypesComponent} from './painting-types/painting-types.component';
import {DamageCategoriesComponent} from './damage-categories/damage-categories.component';
import {CostsComponent} from './costs/costs.component';

const routes: Routes = [
  {
    path: 'painting-types',
    component: PaintingtypesComponent
  },
  {
    path: 'damage-categories',
    component: DamageCategoriesComponent
  },
  {
    path: 'damage-depth-levels',
    component: DamageDepthLevelsComponent
  },
  {
    path: 'costs',
    component: CostsComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
