import { Component } from '@angular/core';
import { MenuItem } from 'primeng/api/menuitem';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  items1: MenuItem[];
  title = 'workartweb';

  ngOnInit(){
    
    this.items1 = [
      {label: 'Painting Types', icon: 'pi pi-fw pi-home', routerLink: 'painting-types'},
      {label: 'Damage Categories', icon: 'pi pi-fw pi-calendar', routerLink: 'damage-categories'},
      {label: 'Damage Depth Levels', icon: 'pi pi-fw pi-pencil', routerLink: 'damage-depth-levels'},
      {label: 'Costs', icon: 'pi pi-fw pi-file', routerLink: 'costs'}
  ];

  }
}
