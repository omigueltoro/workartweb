import { Component, OnInit } from '@angular/core';
import { DamageCategory } from 'src/models/DamageCategory';
import { DamageCategoryService } from '../service/damage-category/damage-category.service';
import { MenuItem, ConfirmationService } from 'primeng/api';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-damage-categories',
  templateUrl: './damage-categories.component.html',
  styleUrls: ['./damage-categories.component.css'],
})
export class DamageCategoriesComponent implements OnInit {
  
  public title : string = "Damage Categories";
  damageCategories : DamageCategory[];
  cols : any [];
  items: MenuItem [];
  displayDCategorySaveDialog : boolean = false;
  damageCategory : DamageCategory = {
    id: null,
    descripcion : null
  };
  selectedDamageCategory: DamageCategory = {
    id: null,
    descripcion : null
  }

  constructor(private DamageCategoriesService: DamageCategoryService, private messageService: MessageService, private confirmService : ConfirmationService) { }

  ngOnInit(): void {
    this.getAll();
    this.cols = [
      {field: "id", header: "Damage category"},
      {field: "descripcion", header: "Description"},
    ];
    this.items = [
      {
        label : "New",
        icon : "pi pi-fw pi-plus",
        command : () => this.showSaveDialog(false)
      },
      {
        label : "Edit",
        icon : "pi pi-fw pi-pencil",
        command : () => this.selectedDamageCategory.id !=null ? this.showSaveDialog(true) : this.messageService.add({severity: 'warn', summary: "Result", detail: "Item not selected"})
      },
      {
        label : "Delete",
        icon : "pi pi-fw pi-times",
        command : () => this.delete()
      }
    ]
  }

  getAll(){
    this.DamageCategoriesService.getAll().subscribe(
      (result: any) => {
        let DamageCategories : DamageCategory[] = [];
        for(let i =0; i < result.length; i++){
          let damageCategory = result[i] as DamageCategory;
          DamageCategories.push(damageCategory);
        }
        this.damageCategories = DamageCategories;
      },
      error => {
        console.log(error);
      }
      
    )
  }
  showSaveDialog(editar:boolean): void {
    if(editar){
      this.damageCategory = this.selectedDamageCategory;
    }
    else{
      this.damageCategory = {
        id: null,
        descripcion : null
      };
    }
    this.displayDCategorySaveDialog = true;
  }
  save(){
    this.DamageCategoriesService.save(this.damageCategory).subscribe(
      (result:any) => {
        let damageCategory = result as DamageCategory;
        let index = this.damageCategories.findIndex(p => p.id == damageCategory.id);
        index != -1 ? this.damageCategories[index] = damageCategory : this.damageCategories.push(damageCategory);
        this.messageService.add({severity: 'success', summary: "Result", detail: "Successful transaction"})
        this.displayDCategorySaveDialog = false;
      },
      error => {
        console.log(error);
      }
    )
  }
  delete(){
    if(this.selectedDamageCategory == null || this.selectedDamageCategory.id == null){
      this.messageService.add({severity: 'warn', summary: "Result", detail: "Item not selected"})
      return;
    }
    this.confirmService.confirm({
      message: "Are you sure you want to delete the record?",
      accept : () => {
        this.DamageCategoriesService.delete(this.selectedDamageCategory.id).subscribe(
          (result : any) => {
            this.messageService.add({severity: 'success', summary: "Result", detail: "Successful transaction"})
            this.deleteObject(result.id)
          }
        )
      }
    })
  }

  deleteObject (id:number) {
    let index = this.damageCategories.findIndex(p => p.id == id);
    if(index != -1){
      this.damageCategories.splice(index, 1)

    }
  }
}
