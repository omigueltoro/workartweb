import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DamageCategoriesComponent } from './damage-categories.component';

describe('DamageCategoriesComponent', () => {
  let component: DamageCategoriesComponent;
  let fixture: ComponentFixture<DamageCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DamageCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DamageCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
