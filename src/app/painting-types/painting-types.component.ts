import { Component, OnInit } from '@angular/core';
import { PaintingType } from 'src/models/PaintingType';
import { PaintingTypeService } from '../service/painting-type/painting-type.service';
import { MenuItem, ConfirmationService } from 'primeng/api';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-paintingtypes',
  templateUrl: './painting-types.component.html',
  styleUrls: ['./painting-types.component.css'],
})
export class PaintingtypesComponent implements OnInit {
  
  public title : string = "Painting Types";
  paintingtypes : PaintingType[];
  cols : any [];
  items: MenuItem [];
  displaySaveDialog : boolean = false;
  paintingtype : PaintingType = {
    id: null,
    descripcion : null
  };
  selectedPaintingType: PaintingType = {
    id: null,
    descripcion : null
  }

  constructor(private paintingTypesService: PaintingTypeService, private messageService: MessageService, private confirmService : ConfirmationService) { }

  ngOnInit(): void {
    this.getAll();
    this.cols = [
      {field: "id", header: "Painting type"},
      {field: "descripcion", header: "Description"},
    ];
    this.items = [
      {
        label : "New",
        icon : "pi pi-fw pi-plus",
        command : () => this.showSaveDialog(false)
      },
      {
        label : "Edit",
        icon : "pi pi-fw pi-pencil",
        command : () => this.selectedPaintingType.id !=null ? this.showSaveDialog(true) : this.messageService.add({severity: 'warn', summary: "Result", detail: "Item not selected"})
      },
      {
        label : "Delete",
        icon : "pi pi-fw pi-times",
        command : () => this.delete()
      }
    ]
  }

  getAll(){
    this.paintingTypesService.getAll().subscribe(
      (result: any) => {
        let paintingtypes : PaintingType[] = [];
        for(let i =0; i < result.length; i++){
          let paintingtype = result[i] as PaintingType;
          paintingtypes.push(paintingtype);
        }
        this.paintingtypes = paintingtypes;
      },
      error => {
        console.log(error);
      }
      
    )
  }
  showSaveDialog(editar:boolean): void {
    if(editar){
      this.paintingtype = this.selectedPaintingType;
    }
    else{
      this.paintingtype = {
        id: null,
        descripcion : null
      };
    }
    this.displaySaveDialog = true;
  }
  save(){
    this.paintingTypesService.save(this.paintingtype).subscribe(
      (result:any) => {
        let paintingtype = result as PaintingType;
        let index = this.paintingtypes.findIndex(p => p.id == paintingtype.id);
        index != -1 ? this.paintingtypes[index] = paintingtype : this.paintingtypes.push(paintingtype);
        this.messageService.add({severity: 'success', summary: "Result", detail: "Successful transaction"})
        this.displaySaveDialog = false;
      },
      error => {
        console.log(error);
      }
    )
  }
  delete(){
    if(this.selectedPaintingType == null || this.selectedPaintingType.id == null){
      this.messageService.add({severity: 'warn', summary: "Result", detail: "Item not selected"})
      return;
    }
    this.confirmService.confirm({
      message: "Are you sure you want to delete the record?",
      accept : () => {
        this.paintingTypesService.delete(this.selectedPaintingType.id).subscribe(
          (result : any) => {
            this.messageService.add({severity: 'success', summary: "Result", detail: "Successful transaction"})
            this.deleteObject(result.id)
          }
        )
      }
    })
  }

  deleteObject (id:number) {
    let index = this.paintingtypes.findIndex(p => p.id == id);
    if(index != -1){
      this.paintingtypes.splice(index, 1)

    }
  }
}
